const DoublyLinkedList = require('./linkedlists/doubly')
/**
 * Stack Implementation using Doubly Linked Lists
 * LIFO - Last In First Out
 *
 * Time Complexities
 *
 * pop: O(1)
 * push: O(1)
 * print: O(n)
 */
class Stack {
  constructor() {
    this.stack = new DoublyLinkedList()
  }

  print() {
    this.stack.print()
  }

  push(key) {
    this.stack.add(key)
  }

  pop() {
    if (this.stack.length < 1) return false

    const popNode = this.stack.tail.prev
    const newEnd = popNode.prev
    newEnd.next = this.stack.tail
    this.stack.tail.prev = newEnd

    this.stack.length--
    return popNode.key
  }
}

module.exports = Stack

const stack = new Stack()

stack.push(1)
stack.push(2)
stack.push(3)
stack.push(4)
stack.push(5)
stack.push(6)

stack.print()

let pop = true

while (pop) {
  pop = stack.pop()
  console.log(pop)
  stack.print()
}
