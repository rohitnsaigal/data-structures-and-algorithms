class Hashtable {
  constructor() {
    this.table = []
    for (let i = 0; i < 10; i++) this.table.push(null)
    console.log('yo')
  }

  hashFunction(key) {
    return key % this.table.length
  }

  set(key, value) {
    const hashedKey = this.hashFunction(key)

    const hashedKeyEntry = this.table[hashedKey]

    if (!hashedKeyEntry) {
      this.table[hashedKey] = [{ key, value }]
    } else {
      let notFound = true,
        i = 0

      while (notFound && i < hashedKeyEntry.length) {
        const currentHashEntryNode = hashedKeyEntry[i]
        if (currentHashEntryNode.key === key) {
          this.table[hashedKey][i].value = value
          notFound = false
        }
        i++
      }

      if (notFound) this.table[hashedKey].push({ key, value })
    }
  }

  get(key) {
    const hashedKey = this.hashFunction(key)

    const hashedKeyEntry = this.table[hashedKey]

    if (!hashedKeyEntry) return null

    for (let i = 0; i < hashedKeyEntry.length; i++)
      if (hashedKeyEntry[i].key === key) return hashedKeyEntry[i].value

    return null
  }
}

module.exports = Hashtable
