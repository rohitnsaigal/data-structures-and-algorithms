const SinglyLinkedList = require('../linkedlists/singly.js')
/**
 * Queue Implementation using Singly Linked Lists
 * FILO - First In First Out
 *
 * Time Complexities
 *
 * pop: O(1)
 * push: O(1)
 * print: O(n)
 */
class Queue {
  constructor() {
    this.queue = new SinglyLinkedList()
  }

  push(key) {
    this.queue.add(key)
  }
  pop() {
    if (this.queue.length === 0) return

    const newNext = this.queue.head.next.next
    const popValue = this.queue.head.next.key
    this.queue.head.next = newNext
    this.queue.length--
    return popValue
  }

  print() {
    this.queue.print()
  }
}

const q = new Queue()

q.push(1)
q.push(2)
q.push(3)
q.push(4)
q.push(5)
q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()

q.print()
q.pop()
q.print()
