const Tree = require('./tree')
/**
 * Red Black Tree
 *
 *
 * Time complexities
 * Search (contains): O(log(n))
 * Insert (add): O(log(n))
 * Deletion (remove): O(log(n))
 *
 *
 *
 *
 *
 *
 * Main Properties:
 * - self-balancing BST
 * - external (no child) property: Every external node is black
 * - depth property: all external nodes have same black depth
 * - red property: children of a red node must be black
 *
 * Use Cases:
 * Most operations take O(h) time where h is height
 * Worst case time is O(n) since O(h) is only if tree is balanced after inserts/deletes
 *
 * Redblack trees are better suited for frequent inserts/deletions
 *
 * Interesting points
 * 1. black is # black nodes on a path from root to leaf; RB tree of height h has black height >= h/2
 * 2. Height <= 2 * log(n+1)
 * 3. All leaves are black (null)
 * 4. black depth = # black nodes from root to that node
 * 5.
 */

class Node {
  constructor(data) {
    this.data = data
    this.left = null
    this.right = null

    this.red = false
  }
}

class RedBlack extends Tree {
  getHeight(node) {}

  remove(data) {}

  contains(data) {
    return this._contains({ node: this.root, data })
  }
  _contains({ node, data }) {
    if (!node) return false
    if (data === node.data) return true

    return this._contains({
      node: data > node.data ? node.right : node.left,
      data,
    })
  }

  add(data) {}

  insert(data) {}
}

module.exports = { RedBlack }

// const tree2 = new RedBlack()
// tree2.add(1)
// tree2.add(2)
// tree2.add(3)
// // tree2.print()

// tree2.add(4)
// // tree2.print()

// tree2.add(5)
// // tree2.print()

// tree2.add(6)

// // tree2.print()
// tree2.add(7)

// tree2.add(8)
// tree2.add(9)
// tree2.add(10)
// tree2.add(11)
// tree2.add(12)
// tree2.add(13)
// tree2.add(14)
// tree2.add(15)
// tree2.print()

// tree2.remove(15)
// tree2.print()

// tree2.remove(14)
// tree2.print()
// tree2.remove(12)
// tree2.print()
