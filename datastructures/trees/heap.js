/**
 * Min Heap
 */
class Heap {
  constructor() {
    this.heap = []
    this.size = 0
  }

  swap(a, b) {
    const toSwap = this.heap[a]
    this.heap[a] = this.heap[b]
    this.heap[b] = toSwap
    return
  }

  compare(a, b) {
    return this.heap[a] > this.heap[b] ? 1 : -1
  }
  getHeight() {
    return Math.floor(Math.log2(this.size))
  }

  buildEmptryTree(rows, columns) {
    const tree = []
    for (let i = 0; i < rows; i++) {
      const newLevel = []
      for (let j = 0; j < columns; j++) newLevel.push('--')
      tree.push(newLevel)
    }
    return tree
  }

  printKey({ idx, pad = 0 }) {
    const key = this.heap[idx]
    let s = `${key}`
    if (s.length === 1) s = '0' + s
    while (key.length < pad) {
      s = ' ' + s + ' '
    }
    return s
  }
  print() {
    const height = this.getHeight()
    const rows = height + 1
    const columns = 2 ** rows - 1
    const tree = this.buildEmptryTree(rows, columns)

    const traverse = (idx, depth, lo, hi) => {
      if (!this.heap[idx]) return

      const m = Math.floor((lo + hi) / 2)
      tree[depth][m] = this.printKey({ idx })
      traverse(2 * idx + 1, depth + 1, lo, m)
      traverse(2 * idx + 2, depth + 1, m, hi)
    }

    traverse(0, 0, 0, columns)
    tree.forEach((t) => console.log(t.join(' ')))
  }

  insert(key) {
    // 1. Add new node to the next open spot in the complete binary tree (end of array represenatation)

    this.heap.push(key)
    // 2. Perform swaps until it is placed in its correct spot
    let insertNode = this.size
    this.size++
    while (insertNode >= 0) {
      const parent = Math.floor(insertNode / 2)

      if (this.compare(parent, insertNode) === 1) {
        this.swap(parent, insertNode)
        insertNode = parent
      } else {
        insertNode = -1
      }
    }
  }

  remove() {
    // 1. Remove the last spot filled in the copmlete binary tree
    const toSwap = this.heap.pop()
    this.size--

    // 2. Replace top with element we removed and shift the array so its satisfies heap property
    const removedKey = this.heap[0]
    this.heap[0] = toSwap
    let curr = 0
    while (curr < this.size) {
      const left = 2 * curr + 1
      const right = 2 * curr + 2

      const smaller = this.compare(left, right) === -1 ? left : right

      if (this.compare(curr, smaller) === 1) {
        this.swap(curr, smaller)
        curr = smaller
      } else curr = this.size
    }

    return removedKey
  }
}

const heap = new Heap()

const inserts = [20, 50, 10, 16, 08, 15, 02]

inserts.forEach((i) => heap.insert(i))
heap.print()

const removed = heap.remove()
console.log(removed)
heap.print()
