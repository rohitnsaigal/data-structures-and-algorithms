/**
 * Implemntation of an Unablanced binary tree to understand the fundamnetals behind binary trees
 * decided not to impliment delete as there is not real use case for this.... maybe in the future i will implement this
 */

class Node {
  constructor(data, idx) {
    this.data = data
    // children[0] is left child, children[1] is right child
    this.left = null
    this.right = null
    this.idx = idx
  }
}

class UnbalancedBinaryTree {
  constructor() {
    this.root = null
    this.maxIdx
  }

  addNode(data) {
    //create node to add
    if (!this.root) {
      this.root = new Node(data, 0)
      this.maxIdx = 0
      return
    }
    return this.add({ data })
  }
  add({ data, currentNode = this.root }) {
    //in the case that the value we are adding is less than our
    // current
    if (data < currentNode.data) {
      if (!currentNode.left) {
        const idx = 2 * currentNode.idx + 1
        if (this.maxIdx < idx) this.maxIdx = idx
        currentNode.left = new Node(data, idx)

        return
      }
      return this.add({ data, currentNode: currentNode.left })
    }

    if (data > currentNode.data) {
      if (!currentNode.right) {
        const idx = 2 * currentNode.idx + 2
        if (this.maxIdx < idx) this.maxIdx = idx
        currentNode.right = new Node(data, idx)
        return
      }
      return this.add({ data, currentNode: currentNode.right })
    }
  }

  getDepth() {
    return Math.floor(Math.log2(this.maxIdx + 1)) + 1
  }

  getDepthOfNode(nodeIdx) {
    return Math.floor(Math.log2(nodeIdx + 1)) + 1
  }

  getHeight() {
    return Math.floor(Math.log2(this.maxIdx + 1))
  }

  buildEmptryTree(rows, columns) {
    const tree = []
    for (let i = 0; i < rows; i++) {
      const newLevel = []
      for (let j = 0; j < columns; j++) newLevel.push('-')
      tree.push(newLevel)
    }
    return tree
  }

  simplePrint() {
    const binaryTree = this.arrayRepresentation()
    if (!binaryTree.length) {
      console.log('Tree is empty')
      return
    }
    const height = this.getHeight()
    const rows = height + 1
    const columns = 2 ** (height + 1) - 1
    const tree = this.buildEmptryTree(rows, columns)

    const popTree = (left, right, depth, i) => {
      if (i >= binaryTree.length) return
      const m = Math.floor((left + right) / 2)

      tree[depth][m] = binaryTree[i]
      popTree(left, m, depth + 1, 2 * i + 1)
      popTree(m, right, depth + 1, 2 * i + 2)
    }

    popTree(0, columns, 0, 0)
    tree.forEach((line) => console.log(line.join(' ')))
  }

  printLeet() {
    const binaryTree = this.arrayRepresentation()
    if (!binaryTree.length) {
      console.log('Tree is empty')
      return
    }

    const height = this.getHeight()
    const rows = height + 1
    const columns = 2 ** (height + 1) - 1
    const tree = this.buildEmptryTree(rows, columns)

    let row = 0
    let column = (columns - 1) / 2
    tree[row][column] = binaryTree[0]

    const populateTree = (position, tree, r, c) => {
      const left = 2 * position + 1
      const right = 2 * position + 2
      console.log({ position, left, right, r, c })
      let nextc, nextr
      if (binaryTree[left]) {
        nextc = c - 2 ** (height - r - 1)
        nextr = r + 1

        tree[nextr][nextc] = binaryTree[left]
        populateTree(left, tree, nextr, nextc)
      }
      if (binaryTree[right]) {
        nextc = c + 2 ** (height - r - 1)
        nextr = r + 1
        console.log(c)

        tree[nextr][nextc] = binaryTree[right]
        populateTree(right, tree, nextr, nextc)
      }
      return
    }

    tree.forEach((line) => console.log(line.join(' ')))
  }

  arrayRepresentation() {
    const nodes = []
    const depth = this.getDepth()
    for (let i = 0; i < 2 ** depth - 1; i++) nodes.push('-')

    const helpPrint = (pos, node, nodes) => {
      nodes[pos] = node.data

      if (!node.left && !node.right) return
      else if (!node.left) helpPrint(2 * pos + 2, node.right, nodes)
      else if (!node.right) helpPrint(2 * pos + 1, node.left, nodes)
      else {
        helpPrint(2 * pos + 2, node.right, nodes)
        helpPrint(2 * pos + 1, node.left, nodes)
      }
    }

    helpPrint(0, this.root, nodes)
    return nodes
  }
}

const tree = new UnbalancedBinaryTree()
tree.addNode(3)

tree.addNode(2)
tree.addNode(1)
tree.addNode(5)
tree.addNode(6)
tree.addNode(0)

tree.simplePrint()
