/**
 * Create a data structure that allows us to detrmine the minimum value in any specified interval of that array
 *
 * Segment tree solves this easily
 */

const _buildSegmentTree = (arr, start, end, node, segmentTree) => {
  if (start == end) {
    segmentTree[node] = { value: arr[start], range: [start, end] }
    return
  }

  const middle = Math.floor((start + end) / 2)
  _buildSegmentTree(arr, start, middle, 2 * node + 1, segmentTree)
  _buildSegmentTree(arr, middle + 1, end, 2 * node + 2, segmentTree)

  segmentTree[node] = {
    range: [start, end],
    value: Math.min(
      segmentTree[2 * node + 1].value,
      segmentTree[2 * node + 2].value,
    ),
  }
}

const buildSegmentTree = (arr) => {
  const treeSize = 2 ** Math.ceil(Math.log2(arr.length))
  const segmentTree = []
  for (let i = 0; i < treeSize; i += 1) {
    segmentTree.push({ value: Infinity })
  }
  _buildSegmentTree(arr, 0, arr.length - 1, 0, segmentTree)
  return segmentTree
}

const _findMinimumInRange = ({ queryRange, arr, segmentTree, node }) => {
  const { value, range } = segmentTree[node]

  if (range[0] >= queryRange[0] && range[1] <= queryRange[1]) return value
  if (queryRange[0] > range[1] || queryRange[1] < range[0]) return Infinity

  const leftNode = _findMinimumInRange({
    queryRange,
    arr,
    segmentTree,
    node: 2 * node + 1,
  })
  const rightNode = _findMinimumInRange({
    queryRange,
    arr,
    segmentTree,
    node: 2 * node + 2,
  })
  return Math.min(leftNode, rightNode)
}
const findMinimumInRange = ({ arr, range }) => {
  const segmentTree = buildSegmentTree(arr)
  return _findMinimumInRange({ queryRange: range, arr, segmentTree, node: 0 })
}

const runBuildTests = (func) => {
  const testCases = [[-1, 2, 4, 0]]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

runBuildTests(buildSegmentTree)

runTests(findMinimumInRange)
