const Tree = require('./tree')
/**
 * AVL tree (Adelson-Velsky and Landis)
 * named after two soviet inventors
 *
 *
 * Time complexities
 * Search (contains): O(log(n))
 * Insert (add): O(log(n))
 * Deletion (remove): O(log(n))
 *
 *
 * Use Cases:
 * More strictly balanced then Red-black trees so better for searches
 */

class Node {
  constructor(data) {
    this.data = data
    this.left = null
    this.height = 0
    this.bf = null
    this.right = null
  }
}

class AVL extends Tree {
  getHeight(node) {
    return node ? node.height : -1
  }

  findLargest(node) {
    while (node.right) {
      node = node.right
    }
    return node.data
  }

  findSmallest(node) {
    while (node.left) {
      node = node.left
    }
    return node.data
  }

  remove(data) {
    if (this.contains(data)) {
      this.root = this._remove({ node: this.root, data })
    }

    return false
  }

  _remove({ node, data }) {
    if (node === null) return null
    if (node.data === data) {
      /**
       * 1. theres no right child, so assign left child as new successor
       * (2.) no left or right child, assign null (in this case left child will be null so first case handles it)
       */
      if (!node.right) return node.left
      /**
       * 2. theres no left child, assign right child as new successor
       */ else if (!node.left) return node.right
      else {
        /**
         * 4. there are two children so if right subtree height > left subtree height
         * A) replace the node with the smallest element from the right subtree
         * ELSE
         * B) replace the node with the largest element grom the left sub tree
         *
         * remove the successor node
         */
        const { left, right } = node
        if (left.height > right.height) {
          const successor = this.findLargest(node.left)
          node.data = successor
          node.left = this._remove({ node: node.left, data: successor })
        } else {
          const successor = this.findSmallest(node.right)
          node.data = successor
          node.right = this._remove({ node: node.right, data: successor })
        }
      }
    } else if (data < node.data) {
      // we haven't found the node yet,  but its in the left subtree
      node.left = this._remove({ node: node.left, data })
    } else {
      // we haven't found the node yet,  but its in the right subtree
      node.right = this._remove({ node: node.right, data })
    }

    this.recalculateHeight(node)

    return this.balance(node)
  }

  _contains(node, data) {
    if (!node) return false
    else if (data === node.data) return true
    else if (data < node.data) return this._contains(node.left, data)
    else if (data > node.data) return this._contains(node.right, data)
  }

  contains(data) {
    return this._contains(this.root, data)
  }

  getBalanceFactor(node) {
    return node ? this.getHeight(node.right) - this.getHeight(node.left) : 0
  }

  recalculateHeight(node) {
    const rightHeight = this.getHeight(node.right)
    const leftHeight = this.getHeight(node.left)
    node.height = Math.max(leftHeight, rightHeight) + 1
    node.bf = this.getBalanceFactor(node)
  }

  rotateRight(A) {
    const B = A.left
    A.left = B.right
    B.right = A

    //update heights
    this.recalculateHeight(A)
    this.recalculateHeight(B)
    return B
  }

  rotateLeft(A) {
    const B = A.right
    A.right = B.left
    B.left = A

    // udpate heights
    this.recalculateHeight(A)
    this.recalculateHeight(B)
    return B
  }

  add(data) {
    this.insert(data)
  }

  insert(data) {
    this.root = this.insertNode({ node: this.root, data })

    // balance it now
  }

  insertNode({ node, data }) {
    if (!node) return new Node(data)
    if (data < node.data) node.left = this.insertNode({ node: node.left, data })
    if (data > node.data)
      node.right = this.insertNode({ node: node.right, data })

    this.recalculateHeight(node)

    return this.balance(node)
  }

  balance(node) {
    if (node.bf == -2) {
      if (node.left.bf == -1) {
        return this.rotateRight(node)
      } else if (node.left.bf == 1) {
        node.left = this.rotateLeft(node.left)
        return this.rotateRight(node)
      }
    } else if (node.bf == 2) {
      if (node.right.bf == 1) {
        return this.rotateLeft(node)
      } else if (node.right.bf == -1) {
        node.right = this.rotateRight(node.right)
        return this.rotateLeft(node)
      }
    }
    return node
  }
}

module.exports = { AVL }
// //rotate left

const rotateLeftTree = new AVL()
rotateLeftTree.insert(10)
rotateLeftTree.insert(20)
rotateLeftTree.insert(30)

// rotateLeftTree.print()

//rotate right

const rotateRightTree = new AVL()
rotateRightTree.add(30)
rotateRightTree.add(20)
rotateRightTree.add(10)

// rotateRightTree.print()

const rotateRightLeftTree = new AVL()
rotateRightLeftTree.add(10)
rotateRightLeftTree.add(30)
rotateRightLeftTree.add(20)
rotateRightLeftTree.add(40)
rotateRightLeftTree.remove(20)
rotateRightLeftTree.print()

const rotateLeftRightTree = new AVL()
rotateLeftRightTree.add(30)
rotateLeftRightTree.add(10)
rotateLeftRightTree.add(20)

// rotateLeftRightTree.print()

const tree2 = new AVL()
tree2.add(1)
tree2.add(2)
tree2.add(3)
// tree2.print()

tree2.add(4)
// tree2.print()

tree2.add(5)
// tree2.print()

tree2.add(6)

// tree2.print()
tree2.add(7)

tree2.add(8)
tree2.add(9)
tree2.add(10)
tree2.add(11)
tree2.add(12)
tree2.add(13)
tree2.add(14)
tree2.add(15)
tree2.print()

tree2.remove(15)
tree2.print()

tree2.remove(14)
tree2.print()
tree2.remove(12)
tree2.print()
