class Tree {
  constructor() {
    this.root = null
  }

  calculateHeight({ node = this.root }) {
    if (!node) return 0

    return (
      1 +
      Math.max(
        this.calculateHeight({ node: node.left }),
        this.calculateHeight({ node: node.right }),
      )
    )
  }

  getEmptyTree(height) {
    const rows = height
    const columns = 2 ** rows - 1
    const tree = []
    for (let i = 0; i < rows; i++) {
      const row = []
      for (let j = 0; j < columns; j++) row.push('-')
      tree.push(row)
    }
    return tree
    s
  }

  print() {
    const height = this.root.height + 1
    const tree = this.getEmptyTree(height)

    const recursePrint = ({ left, right, depth, node }) => {
      if (!node) return
      const m = Math.floor((left + right) / 2)
      tree[depth][m] = node.data

      recursePrint({ left, right: m, depth: depth + 1, node: node.left })
      recursePrint({ left: m, right, depth: depth + 1, node: node.right })
    }

    recursePrint({ left: 0, right: 2 ** height - 1, depth: 0, node: this.root })

    tree.forEach((level) => console.log(level.join(' ')))
    const dash = []
    for (let i = 0; i < 2 ** height; i++) dash.push('*')
    console.log()
    console.log(dash.join('*'))
    console.log()
  }
}

module.exports = Tree
