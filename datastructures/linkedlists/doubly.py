class DoublyLinkedList: 
    def __init__(self):
        self.head = None
        
    def printList(self): 
        s = "HEAD"
        curr = self.head
        while curr:
            s += " <--> " + curr["value"]
            curr = curr["next"]
        print(s)
        

    def addToEnd(self,value):
        if self.head == None:
            self.head = {"next": None, "prev": None, "value": value}
            return
        
        curr = self.head
        while curr["next"]:
            curr = curr["next"]
        curr["next"] = {"next": None,"prev": curr, "value": value}


    def addToFront(self,value):
        if self.head == None:
            self.head = {"next": None, "prev": None, "value": value}
            return

        curr = self.head
        self.head = {"next": curr, "prev": None, "value":value}
        curr["prev"] = self.head

    def addAfterNode(self,addAfterValue, value):
        addAfter = self.head
        while addAfter["value"] != addAfterValue: 
            addAfter = addAfter["next"]

        tempNext = addAfter["next"]
        toAdd = {"next": tempNext,"prev": addAfter, "value": value}
        addAfter["next"] = toAdd
        tempNext["prev"]= toAdd
        
    def getHeadValue(self):
        return self.head["value"]

    def length(self):
        length = 0
        curr = self.head
        if self.head == None: return length

        while curr: 
            length+=1
            curr = curr["next"]
        return length


    
    def recursiveLength(self):
        def helper(curr, length):
            if(curr == None):
                return length
            
            return helper(curr["next"], length+1)
        
        return helper(self.head, 0)


link = DoublyLinkedList()

link.addToEnd("HI")
link.addToEnd("1")
link.addToEnd("2")
link.printList()


link2 = DoublyLinkedList()

link2.addToFront("2")
link2.addToFront("1")
link2.addToFront("HI")
link2.printList()



link3 = DoublyLinkedList()

link3.addToEnd("HI")
link3.addToEnd("1")
link3.addToEnd("3")
link3.addToEnd("5")

link3.addAfterNode("1","2")
link3.addAfterNode("3","4")

link3.printList()


print(link2.recursiveLength())
print(link3.recursiveLength())