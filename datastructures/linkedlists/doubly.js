class Node {
  constructor(key) {
    this.key = key
    this.next = null
    this.prev = null
  }
}
const TAIL = 'TAIL'
const HEAD = 'HEAD'
class DoublyLinkedList {
  constructor() {
    this.head = new Node(HEAD)
    this.tail = new Node(TAIL)
    this.head.next = this.tail
    this.tail.prev = this.head

    this.length = 0
  }

  print() {
    let s = ''
    let node = this.head

    while (node.key !== 'TAIL') {
      s += node.key + ' <--> '
      node = node.next
    }
    s += node.key
    console.log(s)
  }

  add(key) {
    const toAdd = new Node(key)
    const prev = this.tail.prev

    // insert toAdd after Prev
    prev.next = toAdd
    toAdd.prev = prev

    // add tail after toAdd
    toAdd.next = this.tail
    this.tail.prev = toAdd

    this.length++
  }

  find(key) {
    const _find = (node) => {
      if (node.key === key) return node
      if (node.key === TAIL) return false

      return _find(node.next)
    }

    return _find(this.head)
  }

  remove(key) {
    const toRemove = this.find(key)

    if (!toRemove) return

    const { prev, next } = toRemove
    prev.next = next
    next.prev = prev

    this.length--
    return
  }
}

module.exports = DoublyLinkedList

// const list = new DoublyLinkedList()

// list.add('This')
// list.add('is')
// list.add('my')
// list.add('linked')
// list.add('list')
// list.print()

// console.log(list.find('linked').key)
// console.log(list.find('bloop').key)

// list.remove('my')
// list.print()

// list.remove('list')
// list.print()

// list.remove('This')
// list.print()
