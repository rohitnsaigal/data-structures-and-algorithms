class Node {
  constructor(key) {
    this.key = key
    this.next = null
  }
}

class SinglyLinkedList {
  constructor() {
    this.head = new Node('HEAD')
    this.tail = new Node('TAIL')
    this.head.next = this.tail
    this.length = 0
  }

  print() {
    let s = ''
    let node = this.head

    while (node.key !== 'TAIL') {
      s += node.key + ' ---> '
      node = node.next
    }
    s += node.key
    console.log(s)
  }

  add(key) {
    const _add = (node) => {
      if (node.next.key === 'TAIL') {
        const insert = new Node(key)
        node.next = insert
        insert.next = this.tail
        return
      }
      return _add(node.next)
    }
    _add(this.head)
    this.length += 1
  }

  find(key) {
    const _find = (node) => {
      if (node.key === 'TAIL') return { key: 'DOESNT EXIST' }
      if (node.key === key) return node
      return _find(node.next)
    }

    return _find(this.head)
  }

  remove(key) {
    const _remove = (node) => {
      if (node.next.key === 'TAIL') console.log('NOTHING TO REMOVE')
      if (node.next.key === key) {
        const newNext = node.next.next
        node.next = newNext
        this.length--
        return
      }
      return _remove(node.next)
    }

    return _remove(this.head)
  }
}

module.exports = SinglyLinkedList

// const list = new SinglyLinkedList()

// list.add('This')
// list.add('is')
// list.add('my')
// list.add('linked')
// list.add('list')
// list.print()

// console.log(list.find('linked').key)
// console.log(list.find('bloop').key)

// list.remove('my')
// list.print()

// list.remove('list')
// list.print()

// list.remove('This')
// list.print()
