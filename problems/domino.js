const testCases = [
  '6-3',
  '1-2,1-2',
  '1-1,3-5,5-2,2-3,2-4',
  '3-2,2-1,1-4,4-4,5-4,4-2,2-1',
  '5-5,5-5,4-4,5-5,5-5,5-5,5-5,5-5,5-5,5-5',
  '1-1,3-5,5-5,5-4,4-2,1-3',
  '1-2,2-2,3-3,3-4,4-5,1-1,1-2',
]

const domino = (s) => {
  // separate the string into tiles
  const tiles = s.split(',')
  let longestMatch = 1
  let currentMatch = 1
  let stillMatching = false
  for (let i = 0; i < tiles.length - 1; i++) {
    // check to see if the R of the curr tile matches the L of the next
    if (Number(tiles[i][2]) === Number(tiles[i + 1][0])) {
      currentMatch += 1
      stillMatching = true

      // check to see if the current match is now longer
      if (currentMatch > longestMatch) {
        longestMatch = currentMatch
      }
    } else {
      //reset the flags since this tile was not a match
      stillMatching = false
      currentMatch = 1
    }
  }
  return longestMatch
}

for (let i = 0; i < testCases.length; i++) {
  console.log(`TEST CASE NUMBER ${i}`)

  console.log(domino(testCases[i]))

  console.log('END TEST')
}
