const testCases = [
  ['P>E', 'E>R', 'R>U'],
  ['I>N', 'A>I', 'P>A', 'S>P'],
  ['U>N', 'G>A', 'R>Y', 'H>U', 'N>G', 'A>R'],
  ['I>F', 'W>I', 'S>W', 'F>T'],
  ['R>T', 'A>L', 'P>O', 'O>R', 'G>A', 'T>U', 'U>G'],
  ['W>I', 'R>L', 'T>Z', 'Z>E', 'S>W', 'E>R', 'L>A', 'A>N', 'N>D', 'I>T'],
]

const findWord = (rules) => {
  const map = {}
  for (let i = 0; i < rules.length; i++) {
    const [P, E] = rules[i].split('>')

    if (!map[P]) {
      map[P] = {}
    }

    if (!map[E]) {
      map[E] = {}
    }

    map[P].previous = E
    map[E].next = P
  }

  let firstLetter
  const letters = Object.keys(map)

  for (let i = 0; i < letters.length; i++) {
    if (!map[letters[i]].previous) {
      firstLetter = letters[i]
    }
  }

  let currentLetter = map[firstLetter]
  let string = firstLetter
  while (currentLetter.next) {
    string += currentLetter.next
    currentLetter = map[currentLetter.next]
  }

  return string.split('').reverse().join('')
}

for (let i = 0; i < testCases.length; i++) {
  console.log(`TEST CASE NUMBER ${i}`)

  console.log(findWord(testCases[i]))

  console.log('END TEST')
}
