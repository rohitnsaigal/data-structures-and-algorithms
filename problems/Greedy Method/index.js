/**
 *
 * Greedy Method
 *
 *
 * P: A --> B
 * A has many solutions s1,s2...si that get us from A to B
 *
 * Constraint: Get to B in under a certain amount of time
 *
 * Feasible solutionts: only solutions that fit constraints (minimum cost)
 *
 * Optimal Solutions: only solutions that fit constraint
 *
 *
 * Strategies for solving optimization problems are
 *
 * 1. Greedy Method
 * 2. Dynamic Programming
 * 3. Branch and Bound
 *
 *
 *
 */

const greedyPseudoCode = (a, n) => {
  const select = (x) => x
  const feasible = (x) => !!x
  const solution = ''
  for (let i = 0; i < n; i++) {
    x = select(a[i])
    if (feasible(x)) solution += x
  }
  return solution
}

/**
 * Optimized Merge sort
 *
 * If you are given a list of sorted arrays of variable sizes, what is the most efficient way to merge using only 2 way merge
 *
 * Find smallest 2 arrays merge and added merged array to list, removing original arrays
 * repeat until you are left with one array
 */
