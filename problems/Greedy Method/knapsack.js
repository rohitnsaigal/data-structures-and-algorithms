/**
 * Given objects with profits and weights
 * and a bag with weight limit
 *
 * select objects such that max profit is given
 */

const maxProfit = ({ limit, objects }) => {
  for (let i = 0; i < objects.length; i++) {
    objects[i].profitPerWeight = objects[i].profit / objects[i].weight
  }
  objects.sort((a, b) => (a.profitPerWeight < b.profitPerWeight ? 1 : -1))
  console.log(JSON.stringify(objects))

  let totalWeight = 0
  let totalProfit = 0
  const items = []
  while (totalWeight != limit) {
    const { profit, weight, profitPerWeight } = objects.shift()

    if (totalWeight + weight > limit) {
      const amountToAdd = limit - totalWeight
      totalWeight = limit
      totalProfit += amountToAdd * profitPerWeight
    } else {
      totalWeight += weight
      totalProfit += profit
    }
    items.push({ profit, weight, profitPerWeight })
  }

  return { totalProfit, items }
}

const objects = [
  { profit: 10, weight: 2 },
  { profit: 5, weight: 3 },
  { profit: 15, weight: 5 },
  { profit: 7, weight: 7 },
  { profit: 6, weight: 1 },
  { profit: 18, weight: 4 },
  { profit: 3, weight: 1 },
]

console.log(maxProfit({ objects, limit: 15 }))
