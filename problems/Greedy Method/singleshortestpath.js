/**
 *
 * Context:
 * Given a graph G with verticies [A,B,C,D,...] and edges [(A,B),(B,C)(C,D)...] and costs
 * associated to those edges [{edge: (A,B), cost: 5}]
 *
 *
 *
 *
 * Problem:
 * Find shortest path bewteen two verticies
 *
 *
 *
 *
 * Dijkstras:
 * Look at starting vertex, mark the shortest path of the nodes it is connected to
 * mark all other verticies having shortest path of Infinity
 * Now traverse to the next vertext choosing a connected vertex with shortest path
 *
 * now for this vertext u, "relax" its neighbors, v;
 * that is if d[u] + c(u,v) < d[v]; d[v] =  d[u] + c(u,v)
 *
 *
 *Time complexity:
 * worst case scenario for each vertex we look at all edges so O(|V| * |E|) or O(n^2)
 *
 *
 * Draw backs:
 *
 * not always consistent with negative values
 *
 *
 * Bellman Ford:
 *
 * same as djikstras but done on every vertex
 *
 * takes no more than n-1 times to find shortest path
 * if there is something to relax after n-1 times, negative cycle has been found
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
