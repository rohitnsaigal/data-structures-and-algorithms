/**
 * Context:
 * Given a graph G with verticies [A,B,C,D,...] and edges [(A,B),(B,C)(C,D)...] and costs
 * associated to those edges [{edge: (A,B), cost: 5}]
 *
 * Spanning tree is a tree that touches every vertex in a graph just once
 *
 *
 * Problem:
 * Find Minimum cost spanning tree
 *
 *
 * Prims:
 * Only works for connected trees
 * Choose minimum weight edge,
 * look at neighbor edges and choose smallest one
 *
 *
 * Kruskal
 *
 * Can find spanning trees in unconnected graphs
 * Select smallest edge,
 * Keep selecting smallest edge as long sit does't form a cycle among chosen edges
 *
 * Time: O(|V| * |E|) ~ O(n^2)
 *
 * if we used minheap then we take time down to O(n log n)
 *
 *
 *
 */
