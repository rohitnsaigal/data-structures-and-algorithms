/**
 * assume there is a machine that processes each job in 1 unit of time
 *
 * you have a bunch of jobs, choose which jobs to do such that max profit is given
 *
 * solution: sort by profit, pick max profit if schedule allows it, otherwise move on
 */

const scheduleJobsForMaxProfit = (jobs) => {
  jobs.sort((a, b) => (a.profit < b.profit ? 1 : -1))

  let slots = 0
  jobs.forEach(({ deadline }) =>
    deadline > slots ? (slots = deadline) : (slots = slots),
  )

  const openSlots = {}
  for (let i = 0; i < slots; i++) openSlots[i] = 0

  let filled = 0
  let i = 0
  while (filled < slots && i < jobs.length) {
    const { profit, deadline } = jobs[i]
    i++
    let slotToFill = deadline - 1
    while (slotToFill >= 0) {
      if (openSlots[slotToFill] === 0) {
        openSlots[slotToFill] = profit
        slotToFill = -1
        filled++
      } else slotToFill--
    }
  }

  return openSlots
}
const jobs = [
  { profit: 20, deadline: 2 },
  { profit: 15, deadline: 2 },
  { profit: 10, deadline: 1 },
  { profit: 5, deadline: 3 },
  { profit: 1, deadline: 3 },
]

console.log(JSON.stringify(scheduleJobsForMaxProfit(jobs)))
