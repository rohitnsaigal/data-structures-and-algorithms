const testCases = [
  ['eat', 'tea', 'tan', 'ate', 'nat', 'bat', 'fastest', 'testfas'],
]
const groupAnagrams = (strs) => {
  // sort the individual strings
  let sortedStrings = strs.map((str) => ({
    original: str,
    sorted: str.split('').sort().join(''),
  }))

  // sort the entire array
  sortedStrings = sortedStrings.sort((a, b) => (a.sorted > b.sorted ? 1 : -1))

  const groups = []
  let currentGroup = []
  let prev = sortedStrings[0].sorted

  for (let i = 0; i < sortedStrings.length; i++) {
    if (prev === sortedStrings[i].sorted) {
      currentGroup.push(sortedStrings[i].original)
    } else {
      groups.push(currentGroup)
      currentGroup = [sortedStrings[i].original]
      prev = sortedStrings[i].sorted
    }
  }
  groups.push(currentGroup)
  return groups
}

for (let i = 0; i < testCases.length; i++) {
  console.log(`TEST CASE NUMBER ${i}`)

  console.log(JSON.stringify(groupAnagrams(testCases[i])))

  console.log('END TEST')
}
