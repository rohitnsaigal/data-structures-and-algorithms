const testCases = ['XXVII', 'MXL', 'MCDXL']

const convertRomanToInteger = (numeral) => {
  const conversion = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  }
  let total = 0
  for (let i = 0; i < numeral.length - 1; i += 1) {
    let current = numeral[i]
    let next = numeral[i + 1]
    if (conversion[current] < conversion[next]) {
      total -= conversion[current]
    } else {
      total += conversion[current]
    }
  }

  total += conversion[numeral[numeral.length - 1]]

  return total
}

for (let i = 0; i < testCases.length; i++) {
  console.log(`TEST CASE NUMBER ${i}`)

  console.log(convertRomanToInteger(testCases[i]))

  console.log('END TEST')
}
