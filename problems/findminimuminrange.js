/**
 * Create a data structure that allows us to detrmine the minimum value in any specified interval of that array
 *
 * Segment tree solves this easily
 */

const _findMinimumInRange = ({ queryRange, arr, segmentTree, node }) => {
  const { value, range } = segmentTree[node]

  if (range[0] >= queryRange[0] && range[1] <= queryRange[1]) return value
  if (queryRange[0] > range[1] || queryRange[1] < range[0]) return Infinity

  const leftNode = _findMinimumInRange({
    queryRange,
    arr,
    segmentTree,
    node: 2 * node + 1,
  })
  const rightNode = _findMinimumInRange({
    queryRange,
    arr,
    segmentTree,
    node: 2 * node + 2,
  })
  return Math.min(leftNode, rightNode)
}
const findMinimumInRange = ({ arr, range }) => {
  const segmentTree = buildSegmentTree(arr)
  return _findMinimumInRange({ queryRange: range, arr, segmentTree, node: 0 })
}

const runBuildTests = (func) => {
  const testCases = [[-1, 2, 4, 0]]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

const runTests = (func) => {
  const testCases = [
    { range: [1, 2], arr: [-1, 2, 4, 0] },
    { range: [1, 4], arr: [-1, 3, 4, 0, 2, 1] },
    { range: [0, 3], arr: [-1, 3, 4, 0, 2, 1] },
  ]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

runBuildTests(buildSegmentTree)

runTests(findMinimumInRange)
