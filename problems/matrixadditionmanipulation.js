const testCases = [
  {
    numElements: 10,
    queries: [
      [1, 5, 3],
      [4, 8, 7],
      [6, 9, 1],
    ],
  },
]
/**
 *
 * queries m x 3
 * numElements n
 *
 *
 *
 * [1, 5, 3] -> [4, 8, 7] => [1,4,3][4,5,10][6 10 0]
 * O( m*n )
 */

const groupAnagrams = ({ numElements, queries }) => {
  // first initializing my array of zeros
  const array = []
  for (let i = 0; i < numElements; i++) array.push(0)

  let max = 0

  // go through each query
  for (let i = 0; i < queries.length; i++) {
    const [start, end, value] = queries[i]
    console.log()
    let j = start - 1
    while (j < end) {
      array[j] += value
      if (value > max) max = value
      j++
    }

    console.log('reach')
  }

  return Math.max(...array)
}

for (let i = 0; i < testCases.length; i++) {
  console.log(`TEST CASE NUMBER ${i}`)

  console.log(groupAnagrams(testCases[i]))

  console.log('END TEST')
}
