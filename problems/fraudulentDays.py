import copy
from statistics import median;
import math;
def findIndexToAddValueToSortedArray(arr, element):
    lo = 0
    hi = len(arr)
    mid = int((lo+hi)/2)
    if arr[0] > element: return 0
    if arr[-1] < element: return len(arr)
    while(lo!=hi):
        if((arr[mid] == element) | (arr[mid-1] <element & element < arr[mid])):
            return mid
        elif(element < arr[mid]):
            hi = mid 
        elif(element  > arr[mid]):
            lo = mid 
        mid = int((lo+hi)/2)

    return lo
    
def addToSortedArray(arr, element):
    arr = arr[1:]
    idx = findIndexToAddValueToSortedArray(arr, element)
    arr.insert(idx, element)
    return arr


def getMedian(array):
    if(len(array)%2 == 1):
        return array[int((len(array)-1)/2)]
    

    upperMiddleIdx = int(len(array)/2)
    upperMiddle = array[upperMiddleIdx]
    lowerMiddle = array[upperMiddleIdx-1]

    return (lowerMiddle+upperMiddle)/2


    

def getFradulentDays(expenses, d):
    if d>=len(expenses):
        return 0


    # sort trailing payment days O(nlog(n)) 
    sortedTrailingDays = (copy.deepcopy(expenses)[:d])
    sortedTrailingDays.sort()
    # calculate median O(1)
    median = getMedian(sortedTrailingDays) 


    # nlog(n)
    fraudulentDays = 0
    
    # loop can run up to O(n)
    for i in range(d,len(expenses)):
        curr = expenses[i]

        if(curr >= 2* median): fraudulentDays +=1

        # find index in sorted array to add value O(log(n)) +
        # add value at a given index O(n) = 
        # O(n)
        sortedTrailingDays = addToSortedArray(sortedTrailingDays, curr) 
        print(median,sortedTrailingDays, curr)

        # grab the median of the array with the newly added value 
        median = getMedian(sortedTrailingDays)

    # n log(n) 
    return fraudulentDays



def countSort(arr,lo,d):
  # count values for trailing d values 
    valueCount = [0 for i in range(200)]
    for i in range(lo,lo+d):
        currNum = arr[i]
        valueCount[currNum] +=1

    # sum value counts 
    sumCount = []
    sum = 0
    for count in valueCount:
        sum += count
        sumCount.append(sum)

    # populate sorted arrays
    sortedArray = [0 for i in range(d)]
    for i in range(lo,lo+d):
        num = arr[i]
        idx = sumCount[num]
        sumCount[num] -=1
        sortedArray[idx-1] = num

    return sortedArray

def getFraudDaysBinarySearch(expenditure,d):


    fraudDays = 0
    trailingDaysStart = 0
    for i in range(d,len(expenditure)):
        curr = expenditure[i]
        trailingDays = countSort(expenditure,trailingDaysStart,d)
        if(curr >= 2* getMedian(trailingDays)): fraudDays +=1
        trailingDaysStart+=1
    
    return fraudDays



def getMedianSmart(valueCounts, d):
    lidx = math.floor(d/2)
    ridx = math.ceil(d/2)
    
    valuePos = 0
    pos = 0
    while valuePos <= lidx: 
        valuePos += valueCounts[pos]
        pos+=1
    lm = pos - 1
    
    if(d % 2): return lm
        
    valuePos = 0
    pos = 0
    while valuePos <= ridx: 
        valuePos += valueCounts[pos]
        pos+=1
    rm = pos - 1
    return (lm + rm) / 2


    

def activityNotifications(expenditure,d):
    valueCount = [0 for i in range(201)]
    for i in range(d):
        valueCount[expenditure[i]] +=1
    notifications = 0
    for i in range(d, len(expenditure)):
        median =  getMedianSmart(valueCount, d)
        if (expenditure[i] >= 2*median): notifications +=1

        # remove the latest expense in the trailing expenses
        valueCount[expenditure[i-d]] -= 1
        # add the processed expnse to the trailing days count
        valueCount[expenditure[i]] += 1
    return notifications
    





print(activityNotifications([10,20,30,40,50],3))
# print(getFraudDays([10,20,30,40,50],3))
# print(fraudDays([1,2,3,4,4],4))




# arr = [10,20,30,40]
# valueToInsert = 43
# addToSortedArray(arr, valueToInsert)
# print(arr)
