/**
 * PLAIN ENGLISH DESCRIPTION
 * quick sort = pivot
 *
 * Pivot: 1) correct position in final, sorted array 2) items to the left are smaller
 * 3) items to the right are bigger
 *
 *
 * choose pivot and put it aside
 * traverse the array both ways, i going left to right, j going right to left
 * i will look for the first elemnt that is greater than (>) pivot,
 * while j will look for the first element that is smaller than or equal to (<=)the pivot
 * once we find these elements, swap them and then continue
 * stop once i > j, at this point j has become our pivot
 *
 */
const runTests = (func) => {
  const testCases = [[10, 16, 12, 15, 7, 2, 9, 5]]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

const swap = (arr, left, right) => {
  let temp = arr[left]
  arr[left] = arr[right]
  arr[right] = temp
}

const partition = (arr, l, r) => {
  const pivot = arr[l]

  let i = l,
    j = r
  while (i < j) {
    if (arr[i] > pivot && arr[j] <= pivot) {
      swap(arr, i, j)
    }
    while (arr[i] <= pivot) {
      i++
    }
    while (arr[j] > pivot) {
      j--
    }
  }
  swap(arr, l, j)

  return j
}

const quick = (arr, l, r) => {
  //Base Case
  if (l >= r) {
    return
  }

  const p = partition(arr, l, r)

  quick(arr, l, p - 1)
  quick(arr, p + 1, r)
}

const quickSort = (arr) => {
  quick(arr, 0, arr.length - 1)
  return arr
}

runTests(quickSort)

/**
 * ANALYSIS
 *
 * RUNTIME is 0(nlog(n))
 *
 * In order to halve the arrays until they are indivudal elements we must halve them log(n) times
 * This is based on the fundamental properties of logarithm
 *
 * Now in each of these division we perform a merge function that will look at each of the elements
 * in the halves which sum up the total array so n
 *
 * so we create log n smaller arrays to sort but for each of these we look at all the elements in the array
 * giving us nlogn
 *
 */
