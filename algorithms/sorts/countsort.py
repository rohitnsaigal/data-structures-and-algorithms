from re import L


def countSort(arr, maxValue):
    # initialize the array to count the number of each value in the array
    valueCounts = [0 for i in range(maxValue)]

   
    # count the number of each value and record in valueCounts
    for num in arr:
        valueCounts[num]+=1
    
    # sum up the values so that each entry is the sum of all pervious entries and itself 
    sum = 0
    for j in range(len(valueCounts)):
        valueCounts[j] += sum
        sum = valueCounts[j]
        
     # initialize the array that will be sorted 
    sortedArray = [0 for i in range(len(arr))]

    # populate the sorted array
    # Go through each number and find its index in the sorted array
    # by finding its current value count, subtract from the value count to account for this iteration
    # 
    for num in arr:
        idx = valueCounts[num]
        valueCounts[num] -= 1
        sortedArray[idx-1] = num
    return sortedArray


print(countSort([1,4,1,4,2,3,7,8,6], 10))
