/**
 * PLAIN ENGLISH DESCRIPTION
 * Continunously divide the array into halves and then sort those halves back together
 */
const runTests = (func) => {
  const testCases = [[1, 3, 5, 2, -3, 14, 12, -19, 40, 41, 50]]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

const mergeSort = (arr) => {
  //Base Case
  if (arr.length <= 1) {
    return arr
  }

  // split array into 2
  const leftHalf = arr.splice(0, arr.length / 2)
  const rightHalf = arr

  // recursively sort the halves separately
  const sortedLeft = mergeSort(leftHalf)
  const sortedRight = mergeSort(rightHalf)

  // merge the sorted halves
  return merge(sortedLeft, sortedRight)
}

const merge = (left, right) => {
  let merged = []

  // merge while both are populated
  while (left.length && right.length) {
    let l = left[0]
    let r = right[0]

    if (l === r) {
      merged.push(left.shift())
      merged.push(right.shift())
    } else if (l < r) {
      merged.push(left.shift())
    } else if (r < l) {
      merged.push(right.shift())
    }
  }

  // one of the arrays is empty
  // concat the rest of the other array to the merged array
  if (!left.length) {
    merged = merged.concat(right)
    return merged
  } else if (!right.length) {
    merged = merged.concat(left)
    return merged
  }
  return merged
}

runTests(mergeSort)

/**
 * ANALYSIS
 *
 * RUNTIME is 0(nlog(n))
 *
 * In order to halve the arrays until they are indivudal elements we must halve them log(n) times
 * This is based on the fundamental properties of logarithm
 *
 * Now in each of these division we perform a merge function that will look at each of the elements
 * in the halves which sum up the total array so n
 *
 * so we create log n smaller arrays to sort but for each of these we look at all the elements in the array
 * giving us nlogn
 *
 */
