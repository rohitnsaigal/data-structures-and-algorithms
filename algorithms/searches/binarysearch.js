const search = ({ nums, target }) => {
  let left = 0,
    right = nums.length - 1,
    pivot

  if (nums[0] > target) return [-1, 0]
  if (nums[right] < target) return [right, -1]

  while (left != right) {
    pivot = left + Math.floor((right - left) / 2)
    console.log({ left, right, pivot })
    if (nums[pivot] === target) return [pivot, pivot]
    else if (nums[pivot] < target && target < nums[pivot + 1])
      return [pivot, pivot + 1]
    else if (nums[pivot - 1] < target && target < nums[pivot])
      return [pivot - 1, pivot]
    else if (target > nums[pivot]) left = pivot
    else if (target < nums[pivot]) right = pivot
  }
  pivot = l
  if (nums[pivot] < target && target < nums[pivot + 1])
    return [pivot, pivot + 1]
  if (nums[pivot - 1] < target && target < nums[pivot])
    return [pivot - 1, pivot]
}
const runTests = (func) => {
  const testCases = [
    { target: 12, nums: [1, 2, 3, 6, 7, 8, 8, 8, 9, 10, 11, 12, 13] },
    { target: 4, nums: [1, 2, 3, 6, 7, 8, 8, 8, 9, 10, 11, 12, 13] },
    { target: 4, nums: [1, 2, 3, 6, 7, 8, 8, 8, 9, 10, 11, 12] },
    { target: 8, nums: [1, 2, 3, 6, 7, 8, 8, 8, 9, 10, 11, 12] },
    { target: 8, nums: [1, 2, 3, 6, 7, 8, 8, 8, 9, 10, 11, 12, 13] },
  ]
  console.log('start')
  for (let i = 0; i < testCases.length; i++) {
    console.log(testCases[i])
    console.log(`TEST CASE #${i + 1}\n${func(testCases[i])}\n`)
  }
}

//should find the first number smaller than target
var binarySearch = ({ nums, target }) => {
  let l = 0,
    r = nums.length - 1,
    p

  if (target > nums[r]) {
    return -1
  }
  if (target < nums[0]) {
    return 0
  }
  while (l != r) {
    p = l + Math.floor((r - l) / 2)
    console.log({ l, r, p })
    if (nums[p] === target) {
      while (nums[p] >= target) {
        p--
      }
      return p
    } else if (nums[p - 1] < target && nums[p] > target) {
      return p - 1
    } else if (target > nums[p]) {
      l = p + 1
    } else {
      r = p - 1
    }
  }

  if (nums[l] > target) {
    return l - 1
  }
  return l
}

runTests(search)
